<?php 


class Produtos{

	private $nome;

		public function getProduto($id){
			global $pdo;
			$array = array();

			$sql = $pdo->prepare("SELECT * FROM produtos WHERE id = :id");
			$sql->bindValue(":id",$id);
			$sql->execute();

			if($sql->rowCount() > 0){
				$array = $sql->fetch();

				
				}

			return $array;




			}

	public function getProdutos(){
		global $pdo;
		$array = array();

		$sql = $pdo->query("SELECT * FROM produtos");
		$sql->execute();

		if($sql->rowCount() > 0){
			$array = $sql->fetchAll();

			
		}


		return $array;

	}

		public function editarProduto($nome, $unidadeEntrada, $unidadeSaida, $pontoPedido, $pontoEmergencia, $ativo, $id){
		global $pdo;


		$sql = $pdo->prepare("UPDATE produtos SET nome = :nome, uni_entrada = :unidadeEntrada, uni_saida = :unidadeSaida, ponto_pedido = :pontoPedido, ponto_emer = :pontoEmergencia, ativo = :ativo WHERE id = :id");
		$sql->bindValue(':nome',$nome);
		$sql->bindValue(':unidadeEntrada',$unidadeEntrada);
		$sql->bindValue(':unidadeSaida',$unidadeSaida);
		$sql->bindValue(':pontoPedido',$pontoPedido);
		$sql->bindValue(':pontoEmergencia',$pontoEmergencia);	
		$sql->bindValue(':ativo',$ativo);
		$sql->bindValue(':id', $id);
		
		$sql->execute();

		return true;

	}

	public function cadastrarProduto($nome, $unidadeEntrada, $unidadeSaida, $pontoPedido, $pontoEmergencia, $ativo){
		global $pdo;

		$sql = $pdo->prepare("INSERT INTO produtos SET nome = :nome, uni_entrada = :unidadeEntrada, uni_saida = :unidadeSaida, ponto_pedido = :pontoPedido, ponto_emer = :pontoEmergencia, ativo = :ativo");
		$sql->bindValue(':nome',$nome);
		$sql->bindValue(':unidadeEntrada',$unidadeEntrada);
		$sql->bindValue(':unidadeSaida',$unidadeSaida);
		$sql->bindValue(':pontoPedido',$pontoPedido);
		$sql->bindValue(':pontoEmergencia',$pontoEmergencia);
		$sql->bindValue(':ativo',$ativo);
		$sql->execute();

		return true;

	}

	public function excluirProduto($id){


		global $pdo;

		if($this->ProdutoUso($id) == 1){



		return 1;

		}else{


		$sql = $pdo->prepare("DELETE FROM produtos WHERE id = :id");
		$sql->bindValue(":id",$id);
		$sql->execute();

			return 2;
		}



	}

	public function ProdutoUso($id){

		global $pdo;

		$verificao1 = $pdo->prepare("SELECT id_produto FROM itemreqsaida WHERE id_produto = :id_produto");
		$verificao1->bindValue(":id_produto",$id);
		$verificao1->execute();

		if($verificao1->rowCount() > 0){

			return 1;

		}



		$verificao2 = $pdo->prepare("SELECT id_produto FROM itemreqentrada WHERE id_produto = :id_produto");
		$verificao2->bindValue(":id_produto",$id);
		$verificao2->execute();

		if($verificao2->rowCount() > 0){

			return 1;

		}






	}

		public function getNomeProdutos(){
		global $pdo;
		$array = array();

		$sql = $pdo->query("SELECT id, nome FROM produtos WHERE ativo = '1'");
		$sql->execute();

		if($sql->rowCount() > 0){
			$array = $sql->fetchAll();

			
		}

		return $array;

	} 

}


?>