<?php 
require_once 'header.php';
require_once 'aside.php';
require_once 'classes/produto.class.php';
?>

 <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <!-- Start: Topbar -->
      <header id="topbar" class="alt">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-icon">
              <a href="dashboard.html">
                <span class="glyphicon glyphicon-home"></span>
              </a>
            </li>
            <li class="crumb-active">
              <a href="dashboard.html">Cadastros</a>
            </li>
            <li class="crumb-trail">
              <a href="produtos.php">Produtos</a>
            </li>
            <li class="crumb-trail">
              <a href="add-produto.php">Adicionar produto</a>
            </li>
          </ol>
        </div>
      </header>
      <!-- End: Topbar -->      <!-- Begin: Content -->
      <section id="content" class="table-layout animated fadeIn">
        <!-- begin: .tray-center -->
        <div class="tray tray-center">
          <div class="mw1000 center-block">
            <!-- Begin: Admin Form -->
            <div class="admin-form">

                    <div class="row">
                     <?php 
                     
                     $p = new Produtos();

                     if(isset($_POST['nome']) && !empty($_POST['nome'])){

                       $nome = addslashes($_POST['nome']);
                       $unidadeEntrada = addslashes($_POST['uni_entrada']);
                       $unidadeSaida = addslashes($_POST['uni_saida']);
                       $pontoPedido = $_POST['ponto_pedido'];
                       $pontoEmergencia = addslashes($_POST['ponto_emer']);
                       $ativo = addslashes($_POST['ativo']);


                      if(!empty($nome)){

                        if($p->cadastrarProduto($nome, $unidadeEntrada, $unidadeSaida, $pontoPedido, $pontoEmergencia, $ativo)){
                          ?>
                          <div class="alert alert-success alert-dismissable">
                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <i class="fa fa-remove pr10"></i>
                            Produto cadastrado com sucesso !
                          </div>
                        </div>
                          <?php 


                        }else{
                          ?>
                       
                        <div class="alert alert-danger alert-dismissable">
                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <i class="fa fa-remove pr10"></i>
                            O produto já existe
                          </div>
                        </div>
                          <?php
                            }
                        }


                      }

                        ?>

                    </div>
                    <br>
                    <br>
                    <div class="admin-form theme-primary">
                      <div class="panel heading-border panel-primary">
                        <div class="panel-body bg-light">      
                          <div class="section-divider mb40" id="spy1">
                            <span>Cadastro de produtos</span>
                          </div>
                          <form method="POST">
                            <div class="row" id="grid_itens">
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label for="nome"><b>Nome do produto:</b></label>
                                  <input type="text" name="nome" id="nome" class="form-control" required="">
                                </div>
                              </div>
                              <div class="col-md-2">
                                <div class="form-group">
                                  <label for="uni_entrada"><b>Unidade Entrada</b></label>
                                  <select name="uni_entrada" id="uni_entrada" class="form-control">
                                    <option value="UN">UN</option>
                                    <option value="CX">CX</option>
                                    <option value="GAL">GAL</option>
                                    <option value="LIT">LIT</option>
                                  </select>
                                </div>
                              </div>
                              <div class="col-md-2">
                                <div class="form-group">
                                  <label for="uni_saida"><b>Unidade Saída</b></label>
                                  <select name="uni_saida" id="uni_saida" class="form-control">
                                    <option value="UN">UN</option>
                                    <option value="CX">CX</option>
                                    <option value="GAL">GAL</option>
                                    <option value="LIT">LIT</option>
                                  </select>
                                </div>
                              </div>
                              <div class="col-md-2">
                                <div class="form-group">
                                  <label for="senha"><b>Ponto pedido:</b></label>
                                  <div class="col-md-6">
                                    <input type="text" name="ponto_pedido" id="ponto_pedido" class="form-control" >
                                  </div>
                                </div>
                              </div>
                            </div><!-- fim da row -->
                            <div class="row">
                              <div class="col-md-2">

                                <div class="form-group">
                                  <label for="senha"><b>Ponto emergência:</b></label>

                                    <input type="text" name="ponto_emer" id="ponto_emer" class="form-control" >

                                </div>
                              </div>

                              <div class="col-md-2">
                                <div class="form-group">
                                  <label for="ativo"><b>Ativo</b></label>
                                  <select name="ativo" id="ativo" class="form-control">
                                    <option value="1">Sim</option>
                                    <option value="2">Não</option>
                                  </select>
                                </div>
                              </div>
                            </div><!-- fim da row -->
                            <div class="row">
                              <div class="col-md-2">
                                <input type="submit" name="Cadastrar" class="btn btn-success" value="Cadastrar" id="add-campo">
                              </div>
                            </div>
                          </form> 
                

            </div>
          </div>
        </div>
        <!-- end: .tray-center -->
      </section>
      <!-- End: Content -->
    </section>
  </div>
  <!-- End: Main -->

  <style>
  /* demo page styles */
  body { min-height: 2300px; }
  
  .content-header b,
  .admin-form .panel.heading-border:before,
  .admin-form .panel .heading-border:before {
    transition: all 0.7s ease;
  }
  /* responsive demo styles */
  @media (max-width: 800px) {
    .admin-form .panel-body { padding: 18px 12px; }
    .option-group .option { display: block; }
    .option-group .option + .option { margin-top: 8px; }
  }
  </style>
    

  <!-- BEGIN: PAGE SCRIPTS -->

  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script type="text/javascript">
  jQuery(document).ready(function() {

    "use strict";

    // Init Theme Core    
    Core.init();

    // Form Switcher
    $('#form-switcher > button').on('click', function() {
      var btnData = $(this).data('form-layout');
      var btnActive = $('#form-elements-pane .admin-form.active');

      // Remove any existing animations and then fade current form out
      btnActive.removeClass('slideInUp').addClass('animated fadeOutRight animated-shorter');
      // When above exit animation ends remove leftover classes and animate the new form in
      btnActive.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
        btnActive.removeClass('active fadeOutRight animated-shorter');
        $('#' + btnData).addClass('active animated slideInUp animated-shorter')
      });
    });

    // Cache several DOM elements
    var pageHeader = $('.content-header').find('b');
    var adminForm = $('.admin-form');
    var options = adminForm.find('.option');
    var switches = adminForm.find('.switch');
    var buttons = adminForm.find('.button');
    var Panel = adminForm.find('.panel');

    // Form Skin Switcher
    $('#skin-switcher a').on('click', function() {
      var btnData = $(this).data('form-skin');

      $('#skin-switcher a').removeClass('item-active');
      $(this).addClass('item-active')

      adminForm.each(function(i, e) {
        var skins = 'theme-primary theme-info theme-success theme-warning theme-danger theme-alert theme-system theme-dark';
        var panelSkins = 'panel-primary panel-info panel-success panel-warning panel-danger panel-alert panel-system panel-dark';
        $(e).removeClass(skins).addClass('theme-' + btnData);
        Panel.removeClass(panelSkins).addClass('panel-' + btnData);
        pageHeader.removeClass().addClass('text-' + btnData);
      });

      $(options).each(function(i, e) {
        if ($(e).hasClass('block')) {
          $(e).removeClass().addClass('block mt15 option option-' + btnData);
        } else {
          $(e).removeClass().addClass('option option-' + btnData);
        }
      });
      $(switches).each(function(i, ele) {
        if ($(ele).hasClass('switch-round')) {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-round switch-' + btnData);
          } else {
            $(ele).removeClass().addClass('switch switch-round switch-' + btnData);
          }
        } else {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-' + btnData);
          } else {
            $(ele).removeClass().addClass('switch switch-' + btnData);
          }
        }

      });
      buttons.removeClass().addClass('button btn-' + btnData);
    });

    setTimeout(function() {
      adminForm.addClass('theme-primary');
      Panel.addClass('panel-primary');
      pageHeader.addClass('text-primary');

      $(options).each(function(i, e) {
        if ($(e).hasClass('block')) {
          $(e).removeClass().addClass('block mt15 option option-primary');
        } else {
          $(e).removeClass().addClass('option option-primary');
        }
      });
      $(switches).each(function(i, ele) {

        if ($(ele).hasClass('switch-round')) {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-round switch-primary');
          } else {
            $(ele).removeClass().addClass('switch switch-round switch-primary');
          }
        } else {
          if ($(ele).hasClass('block')) {
            $(ele).removeClass().addClass('block mt15 switch switch-primary');
          } else {
            $(ele).removeClass().addClass('switch switch-primary');
          }
        }
      });
      buttons.removeClass().addClass('button btn-primary');
    }, 800);

  });
  </script>
  <!-- END: PAGE SCRIPTS -->

</body>

</html>
