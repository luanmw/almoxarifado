<?php require_once 'header.php'; ?>
 <!-- Start: Sidebar -->
    <aside id="sidebar_left" class="nano nano-light sidebar-light affix">

      <!-- Start: Sidebar Left Content -->
      <div class="sidebar-left-content nano-content">

        <!-- Start: Sidebar Header -->
        <header class="sidebar-header">

          <!-- Sidebar Widget - Menu (slidedown) -->
          <div class="sidebar-widget menu-widget">
            <div class="row text-center mbn">
              <div class="col-xs-4">
                <a href="dashboard.html" class="text-primary" data-toggle="tooltip" data-placement="top" title="Dashboard">
                  <span class="glyphicon glyphicon-home"></span>
                </a>
              </div>
              <div class="col-xs-4">
                <a href="pages_messages.html" class="text-info" data-toggle="tooltip" data-placement="top" title="Messages">
                  <span class="glyphicon glyphicon-inbox"></span>
                </a>
              </div>
              <div class="col-xs-4">
                <a href="pages_profile.html" class="text-alert" data-toggle="tooltip" data-placement="top" title="Tasks">
                  <span class="glyphicon glyphicon-bell"></span>
                </a>
              </div>
              <div class="col-xs-4">
                <a href="pages_timeline.html" class="text-system" data-toggle="tooltip" data-placement="top" title="Activity">
                  <span class="fa fa-desktop"></span>
                </a>
              </div>
              <div class="col-xs-4">
                <a href="pages_profile.html" class="text-danger" data-toggle="tooltip" data-placement="top" title="Settings">
                  <span class="fa fa-gears"></span>
                </a>
              </div>
              <div class="col-xs-4">
                <a href="pages_gallery.html" class="text-warning" data-toggle="tooltip" data-placement="top" title="Cron Jobs">
                  <span class="fa fa-flask"></span>
                </a>
              </div>
            </div>
          </div>

          <!-- Sidebar Widget - Search (hidden) -->
          <div class="sidebar-widget search-widget hidden">
            <div class="input-group">
              <span class="input-group-addon">
                <i class="fa fa-search"></i>
              </span>
              <input type="text" id="sidebar-search" class="form-control" placeholder="Search...">
            </div>
          </div>

        </header>
        <!-- End: Sidebar Header -->

        <!-- Start: Sidebar Menu -->
        <ul class="nav sidebar-menu">
          <li class="sidebar-label pt36">Menu</li>
        <?php if($_SESSION['tipo'] != 1): ?>
          <li>
            <a class="accordion-toggle" href="#">
              <span class="glyphicon glyphicon-plus"></span>
              <span class="sidebar-title">Cadastros</span>
              <span class="caret"></span>
            </a>
            <ul class="nav sub-nav">
              <li>
                <a href="produtos.php">
                  <span class="glyphicon glyphicon-book"></span>Produtos</a>
              </li>
              <li>
                <a href="fornecedores.php">
                  <span class="glyphicon glyphicon-list-alt"></span>Forcecedores</a>
              </li>
              <?php if($_SESSION['tipo'] == 4): ?>                     
                <li>
                  <a href="usuarios.php">
                    <span class="fa fa-group"></span>Usuários</a>
                </li>
              <?php endif;?>
            </ul>
          </li>
          <?php endif;?>
          <li>
            <a class="accordion-toggle " href="#">
              <span class="imoon imoon-checkmark2"></span>
              <span class="sidebar-title">Procedimentos</span>
              <span class="caret"></span>
            </a>
            <ul class="nav sub-nav">
              <?php if($_SESSION['tipo'] != 1): ?>
              <li>
                <a href="entrada-materiais.php">
                  <span class="imoon imoon-arrow-right"></span>Entrada de materiais</a>
              </li>
              <?php endif;?>
              <li>
                <a href="saida-materiais.php">
                  <span class="imoon imoon-arrow-left"></span>Saída de materiais</a>
              </li>
            </ul>
          </li>
          <?php if($_SESSION['tipo'] != 1): ?>
          <li>
            <a class="accordion-toggle " href="#">
              <span class="glyphicon glyphicon-list-alt"></span>
              <span class="sidebar-title">Consultas</span>
              <span class="caret"></span>
            </a>
            <ul class="nav sub-nav">
              
              <li>
                <a href="consulta_saldo.php">
                  <span class="imoon imoon-numbered-list"></span>Saldo Materiais</a>
              </li>
            </ul>
          </li>
          <?php endif;?>
          <?php if($_SESSION['tipo'] != 1): ?>
          <li>
            <a class="accordion-toggle" href="#">
              <span class="fa fa-columns"></span>
              <span class="sidebar-title">Relatórios</span>
              <span class="caret"></span>
            </a>
            <ul class="nav sub-nav">
              <li>
                <a class="accordion-toggle" href="#">
                  <span class="fa fa fa-arrows-h"></span>
                  Físico
                </a>
              </li>
              <li>
                <a class="accordion-toggle" href="#">
                  <span class="fa fa fa-arrows-h"></span>
                  Financeiro
                </a>
              </li>
              <li>
                <a href="relatorio_inventario.php">
                  <span class="fa fa fa-arrows-h"></span>
                  Físico - Financeiro
                </a>
              </li>
                </ul>
              </li>
            </ul>
          </li>
          <?php endif;?>
        </ul>
        <!-- End: Sidebar Menu -->
      </div>
      <!-- End: Sidebar Left Content -->

    </aside>
    <!-- End: Sidebar -->

  
</html>
